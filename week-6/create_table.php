
    <?php
        include "connection.php";
        $table = "CREATE TABLE Employee (id INT(6) PRIMARY KEY,firstname VARCHAR(30),lastname VARCHAR(30),email VARCHAR(50))";
        if (mysqli_query($conn, $table)) {
            echo "Table Employee created successfully";
        } else {
            echo "Error creating table: " . mysqli_error($conn);
        }
    ?>
