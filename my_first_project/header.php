<?php
session_start();
if (isset($_POST['logout'])) {
    session_unset();
    session_destroy();
    header("Location:login.php");
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

</head>

<body>
    <div class="container">
        <nav class="navbar navbar-light bg-light">
            <form class="form-inline" method="POST">
                <a class="btn btn-outline-dark mx-5 px-5" onclick="location.href = 'home.php';" type="button">Home</a>
                <a class="btn btn-outline-secondary mx-5 px-5" onclick="location.href = 'author.php';" type="button">Author</a>
                <a class="btn btn-outline-info px-5 mx-5" onclick="location.href = 'book.php';" type="button">Book</a>
                <button class="btn btn-outline-info px-5 mx-5" type="submit" name="logout">Log out</button>
            </form>
        </nav>
    </div>
</body>

</html>