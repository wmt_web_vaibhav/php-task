<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    protected $fillable = [
        'name', 'phoneno'
    ];
    public function Detail(){
        return $this->hasOne(Detail::class);
    }
}
