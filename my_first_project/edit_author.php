<?php
include "IU_author.php"
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>

    <script>
        $(document).ready(function() {
            $("#form").validate({
                rules: {
                    name: {
                        required: true
                    },
                    date: {
                        required: true,
                        date: true
                    },
                    address: {
                        required: true
                    },
                    mobile: {
                        required: true
                    },
                    description: {
                        required: true
                    }
                }
            })
        });
    </script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Edit Author Detail</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" id="form" action="IU_author.php">
                    <div class="form-group">
                        <label for="">Name</label>
                        <input type="text" name="name" id="name" class="form-control" value="<?php echo $name ?>" aria-describedby="helpId">
                        <small><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Date Of Birth</label>
                        <input type="date" name="date" id="date" class="form-control" value="<?php echo $date ?>" aria-describedby="helpId">
                        <small><?php echo $errr ?></small>
                    </div>

                    <div class="form-group">
                        <label for="">Gender</label>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="<?php echo 'male' ?>" <?php echo ($gender == 'male' ? 'checked' : '') ?>>
                            <label class="form-check-label" for="materialUnchecked">Male</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="<?php echo 'female' ?>" <?php echo ($gender == 'female' ? 'checked' : '') ?>>
                            <label class="form-check-label" for="materialUnchecked">Female</label>
                        </div>
                        <div class="form-check">
                            <input type="radio" class="form-check-input" id="gender" name="gender" value="<?php echo 'other' ?>" <?php echo ($gender == 'other' ? 'checked' : '') ?>>
                            <label class="form-check-label" for="materialChecked">Other</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="">Address</label>
                        <textarea class="form-control" name="address" id="address" rows="3"><?php echo $address ?></textarea>
                        <small><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Mobile No</label>
                        <input type="text" name="mobile" id="mobile" class="form-control" value="<?php echo $mobile ?>" aria-describedby="helpId">
                        <small><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3"><?php echo $description ?></textarea>
                        <small><?php echo $errr ?></small>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="btn" id="btn" class="btn btn-outline-secondary" value="Update" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>