<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Detail;
use Illuminate\Http\Request;

class employeecontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee=employee::all();
        echo "<table border='1'>";
        echo "<tr>";
        echo "<th>ID</th>";
        echo "<th>Name</th>";
        echo "<th>phone_no.</th>";
        echo "</tr>";
        foreach ($employee as $c) {
            echo "<tr>";
            echo "<td>$c->id</td>";
            echo "<td>$c->name</td>";
            echo "<td>$c->phoneno</td>";
            echo "</tr>";
        }
        echo "</table>";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        echo "<h3>CREATE</h3>";
        echo '<form action="/employee" method="POST">';
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="text" name="name"><br>';
        echo '<input type="number" name="phoneno"><br>';
        echo '<input type="submit">';
        echo '</form >';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo "<h3>STORE</h3>";
        employee::create($request->all());
        return redirect()->route('employee.index');
    }


    public function show(Employee $employee)
    {
        echo "<h3>SHOW</h3>";
        echo "<table border='1'>";
        echo "<tr>";
        echo "<th>Name</th>";
        echo "<th>phone_no.</th>";
        echo "</tr>";
        echo "<tr>";
        echo "<td>$employee->name</td>";
        echo "<td>$employee->phoneno</td>";
        echo "</tr>";
        echo "</table>";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        echo "<h3>EDIT: $employee->name</h3>";
        echo "<form action='/employee/$employee->id' method='POST'>";
        echo '<input type="hidden" name="_token" value="' . csrf_token() . '" >';
        echo '<input type="hidden" name="_method" value="PUT">';
        echo "<input type='text' name='name' value='$employee->name'>";
        echo "<input type='number' name='phoneno' value='$employee->phoneno'>";
        echo '<input type="submit">';
        echo '</form >';
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Employee $employee)
    {

        $employee->update($request->all());
        return redirect()->route('employee.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {

        $employee->delete();
        return redirect()->route('employee.index');
    }

    public function other()
    {
    $employee=Detail::find(2);
    echo $employee->Employee;
    }
}
