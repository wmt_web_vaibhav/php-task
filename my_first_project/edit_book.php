<?php
include "IU_book.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
    <script>
        $(document).ready(function() {
            $.validator.addMethod("isbn", function(value, element) {
                return this.optional(element) || /^(?:ISBN(?:-1[03])?:? )?(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$|97[89][0-9]{10}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)(?:97[89][- ]?)?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$/.test(value);
            }, "Please specify a valid isbn number");
            $("#form").validate({
                rules: {
                    title: {
                        required: true
                    },
                    page: {
                        required: true
                    },
                    book_author: {
                        required: true
                    },
                    language: {
                        required: true
                    },
                    cover_img: {
                        required: true
                    },
                    isbn_no: {
                        required: true,
                        isbn: true
                    },
                    description: {
                        required: true
                    }
                }
            })
        });
    </script>
    <style>
        .error {
            color: red;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="text-center card-title">Edit Book Detail </h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <form method="post" id="form" action="IU_book.php">
                    <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" name="title" id="title" class="form-control" value="<?php echo $title ?>" aria-describedby="helpId">
                        <small><? echo $err ?></small>
                    </div>
                    <div class="form-group">
                        <label for="">Page</label>
                        <input type="text" name="page" id="page" class="form-control" value="<?php echo $pages ?>" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Language</label>
                        <input type="text" name="language" id="language" class="form-control" value="<?php echo $language ?>" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">Book Author</label>
                        <select class="custom-select" name="book_author" id="book_author">
                            <option hidden>Select Author Name</option>
                            <?php
                            $sql = "select * from author";
                            $result = $conn->query($sql);
                            if ($result->num_rows > 0) {
                                while ($row = $result->fetch_assoc()) {
                                    echo "<option value=" . $row['id'] . " " . ($book_author == $row['id'] ? 'selected' : '') . ">" . $row['fullname'] . "</option>";
                                }
                            }
                            ?>
                        </select>
                        <span class="error"></span>
                    </div>
                    <div class="form-group">
                        <label for="">Cover image</label>
                        <input type="text" name="cover_img" id="cover_img" class="form-control" value="<?php echo $cover_img ?>" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="">ISBN NO</label>
                        <input type="text" name="isbn_no" id="isbn_no" class="form-control" value="<?php echo $isbn_no ?>" aria-describedby="helpId">
                        <small class="text-muted">Ex: [1-2222-3333-4]</small>
                    </div>
                    <div class="form-group">
                        <label for="">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3" id="comment"><?php echo $description ?></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="btn" id="btn" class="btn btn-success" value="Submit" aria-describedby="helpId">
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>

</html>