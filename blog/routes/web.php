<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/form', function () {

    return view('form');
})->name('profile');
Route::post('/form', function () {

    return "this is post method";
});
Route::put('/form', function () {

    return "this is put method";
});
Route::delete('/form', function () {

    return "this is delete method";
});
Route::any('all', function () {

    return "this is all method";
});
Route::get('form/{search}/', function ($name) {

    echo "this is name :  $name";
})->where('search', '.*');
Route::group(['prefix' => 'privacy'], function () {
    Route::any('all', function () {

        return "this is all method";
    });
    Route::get('form/{name}/{val?}', function ($name,$val=null) {
        echo "this is name :  $name<br>";

        if(null!=$val){
            echo "this is value : $val <br>";
        }

    });
});


Route::get('user', function () {
$x=App\vaibhav::take(4)->orderBy('name','asc')->get();
echo $x;
});
